(function(){

  function waitForIt(selector, time) {
    if(document.querySelector(selector)!=null) {

      var chat_title = document.getElementsByClassName("chat-area col")[0];
      var element = document.createElement("div");
      element.setAttribute("id", "video");
      //element.setAttribute("style", "height: 50%;");

      element.innerHTML = '<div class=\"content\"><video controls id=\"master\" class=\"video-js vjs-default-skin vjs-fullscreen\" poster=\"live_poster.png\" controls data-setup=\"{}\"><source src=\"https://live.lilimit.com:8081/hls1/Aymeric.m3u8\" type=\"application/x-mpegURL\" ></video></div>';

      chat_title.insertBefore(element, chat_title.firstChild);

      return;
    }
    else {
      setTimeout(function() {

        waitForIt(selector, time);
        var player = videojs('master');player.play();player.autoplay(true);

      }, time);
    }
  }

  waitForIt(".toggle-smiley", 1000);
})();
