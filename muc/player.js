(function(){

  function waitForIt(selector, time) {
    if(document.querySelector(selector)!=null) {

      var chat_title = document.getElementsByClassName("chatbox-title__text")[0];
      var element = document.createElement("span");

      element.innerHTML = '<audio id="player" preload="none" src="https://echo.lurk.org:999/echochamber"></audio><div><button onclick="document.getElementById(\'player\').play()">Play</button><button onclick="document.getElementById(\'player\').pause();document.getElementById(\'player\').src=document.getElementById(\'player\').src;">Stop</button><button onclick="document.getElementById(\'player\').volume += 0.1">Vol +</button><button onclick="document.getElementById(\'player\').volume -= 0.1">Vol -</button></div>';

      chat_title.insertAdjacentElement('afterend', element);

      return;
    }
    else {
      setTimeout(function() {

        waitForIt(selector, time);

      }, time);
    }
  }

  waitForIt(".toggle-smiley", 1000);
})();
